import fs from 'fs';
import path from 'path';

export function writeStartFile(info: string, codeDir: string) {
  const codeResoleDir = path.resolve(codeDir); 
  const startFilePath = path.join(codeResoleDir, 'start.sh');

  try {
    if (fs.statSync(startFilePath).isFile()) {
      return '[start.sh] The file already exists and will be used as the startup file.';
    };
  // @ts-ignore
  } catch (e) {}

  fs.writeFileSync(startFilePath, info, {
    mode: '0755'
  });
  return 'The startup file is not found, create a [start.sh] as the startup file.';
}

export const getCommand = function(deployType: string, name: string) {
  const startCommand = 'python3 ./index.py';

  if (deployType === 'container') {
    return {
      command: '["./start.sh"]',
      commandContent: `#!/bin/bash

${startCommand}`,
    }
  }

  if (!name) {
    throw ('functionName is necessary');
  }
  
  return {
    command: `["./${name}/start.sh"]`,
    commandContent: `#!/bin/bash

cd /mnt/auto/${name} && ${startCommand}`,
  }
}

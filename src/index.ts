import { HLogger, ILogger, loadComponent, getCredential, reportComponent } from '@serverless-devs/core';
import _ from 'lodash';
import BaseComponent from './base';
import { getCommand, writeStartFile } from './utils';

const WEB_FRAMEWORK = 'devsapp/web-framework';
const RUNTIME = 'python3';
interface IInputs {
  props: any;
  project: {
    component: string;
    access: string;
    projectName: string;
  };
  appName: string;
  args: string;
  path: any;
}

async function report(inputs: IInputs, command: string) {
  inputs.props.runtime = RUNTIME;
  const credential = await getCredential(inputs.project.access);

  reportComponent('pyramid', {
    command,
    uid: credential.AccountID,
  });
}
export default class Component extends BaseComponent{
  @HLogger('PYRAMID') logger: ILogger;

  async getDeployType() {
    const fcDefault = await loadComponent('devsapp/fc-default');
    return await fcDefault.get({ args: "web-framework" });
  }

  /**
   * 部署应用
   * @param inputs
   */
  async deploy(inputs: IInputs) {
    const propsFunction = inputs.props.function;
    const hasCommand = !!propsFunction?.customContainerConfig?.command;
    if (!hasCommand) {
      const functionName = propsFunction.name || inputs.props.service?.name;

      const deployType = await this.getDeployType();
      const { commandContent, command } = getCommand(deployType, functionName);
      const warn = writeStartFile(commandContent, propsFunction.code.src);
      this.logger.warn(warn);

      propsFunction.customContainerConfig || (propsFunction.customContainerConfig = {});
      propsFunction.customContainerConfig.command = command;
    }

    await report(inputs, 'deploy');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.deploy(inputs);
  }
  /**
   * 删除应用
   * @param inputs
   */
  async remove(inputs: IInputs) {
    await report(inputs, 'remove');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.remove(inputs);
  }
  /**
   * 构建应用
   * @param inputs
   */
  async build(inputs: IInputs) {
    await report(inputs, 'build');

    inputs.props.runtime = 'custom';
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.build(inputs);
  }
  /**
   * 打印日志
   * @param inputs
   */
  async logs(inputs: IInputs) {
    await report(inputs, 'logs');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.logs(inputs);
  }
  /**
   * 指标数据查看
   * @param inputs
   */
  async metrics(inputs: IInputs) {
    await report(inputs, 'metrics');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.metrics(inputs);
  }
  /**
   * 指标部署函数信息
   * @param inputs
   */
  async info(inputs: IInputs) {
    await report(inputs, 'info');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.info(inputs);
  }
  /**
   * 查看Nas目录
   * @param inputs
   */
  async ls(inputs: IInputs) {
    await report(inputs, 'ls');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.ls(inputs);
  }
  /**
   * 移除Nas文件/目录
   * @param inputs
   */
  async rm(inputs: IInputs) {
    await report(inputs, 'rm');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.rm(inputs);
  }
  /**
   * 拷贝文件到Nas/更新操作
   * @param inputs
   */
  async cp(inputs: IInputs) {
    await report(inputs, 'cp');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.cp(inputs);
  }
}
import { ILogger } from '@serverless-devs/core';
import BaseComponent from './base';
interface IInputs {
    props: any;
    project: {
        component: string;
        access: string;
        projectName: string;
    };
    appName: string;
    args: string;
    path: any;
}
export default class Component extends BaseComponent {
    logger: ILogger;
    getDeployType(): Promise<any>;
    /**
     * 部署应用
     * @param inputs
     */
    deploy(inputs: IInputs): Promise<any>;
    /**
     * 删除应用
     * @param inputs
     */
    remove(inputs: IInputs): Promise<any>;
    /**
     * 构建应用
     * @param inputs
     */
    build(inputs: IInputs): Promise<any>;
    /**
     * 打印日志
     * @param inputs
     */
    logs(inputs: IInputs): Promise<any>;
    /**
     * 指标数据查看
     * @param inputs
     */
    metrics(inputs: IInputs): Promise<any>;
    /**
     * 指标部署函数信息
     * @param inputs
     */
    info(inputs: IInputs): Promise<any>;
    /**
     * 查看Nas目录
     * @param inputs
     */
    ls(inputs: IInputs): Promise<any>;
    /**
     * 移除Nas文件/目录
     * @param inputs
     */
    rm(inputs: IInputs): Promise<any>;
    /**
     * 拷贝文件到Nas/更新操作
     * @param inputs
     */
    cp(inputs: IInputs): Promise<any>;
}
export {};

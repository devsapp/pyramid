"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCommand = exports.writeStartFile = void 0;
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
function writeStartFile(info, codeDir) {
    var codeResoleDir = path_1.default.resolve(codeDir);
    var startFilePath = path_1.default.join(codeResoleDir, 'start.sh');
    try {
        if (fs_1.default.statSync(startFilePath).isFile()) {
            return '[start.sh] The file already exists and will be used as the startup file.';
        }
        ;
        // @ts-ignore
    }
    catch (e) { }
    fs_1.default.writeFileSync(startFilePath, info, {
        mode: '0755'
    });
    return 'The startup file is not found, create a [start.sh] as the startup file.';
}
exports.writeStartFile = writeStartFile;
exports.getCommand = function (deployType, name) {
    var startCommand = 'python3 ./index.py';
    if (deployType === 'container') {
        return {
            command: '["./start.sh"]',
            commandContent: "#!/bin/bash\n\n" + startCommand,
        };
    }
    if (!name) {
        throw ('functionName is necessary');
    }
    return {
        command: "[\"./" + name + "/start.sh\"]",
        commandContent: "#!/bin/bash\n\ncd /mnt/auto/" + name + " && " + startCommand,
    };
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsMENBQW9CO0FBQ3BCLDhDQUF3QjtBQUV4QixTQUFnQixjQUFjLENBQUMsSUFBWSxFQUFFLE9BQWU7SUFDMUQsSUFBTSxhQUFhLEdBQUcsY0FBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM1QyxJQUFNLGFBQWEsR0FBRyxjQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUUzRCxJQUFJO1FBQ0YsSUFBSSxZQUFFLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO1lBQ3ZDLE9BQU8sMEVBQTBFLENBQUM7U0FDbkY7UUFBQSxDQUFDO1FBQ0osYUFBYTtLQUNaO0lBQUMsT0FBTyxDQUFDLEVBQUUsR0FBRTtJQUVkLFlBQUUsQ0FBQyxhQUFhLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRTtRQUNwQyxJQUFJLEVBQUUsTUFBTTtLQUNiLENBQUMsQ0FBQztJQUNILE9BQU8seUVBQXlFLENBQUM7QUFDbkYsQ0FBQztBQWZELHdDQWVDO0FBRVksUUFBQSxVQUFVLEdBQUcsVUFBUyxVQUFrQixFQUFFLElBQVk7SUFDakUsSUFBTSxZQUFZLEdBQUcsb0JBQW9CLENBQUM7SUFFMUMsSUFBSSxVQUFVLEtBQUssV0FBVyxFQUFFO1FBQzlCLE9BQU87WUFDTCxPQUFPLEVBQUUsZ0JBQWdCO1lBQ3pCLGNBQWMsRUFBRSxvQkFFcEIsWUFBYztTQUNYLENBQUE7S0FDRjtJQUVELElBQUksQ0FBQyxJQUFJLEVBQUU7UUFDVCxNQUFNLENBQUMsMkJBQTJCLENBQUMsQ0FBQztLQUNyQztJQUVELE9BQU87UUFDTCxPQUFPLEVBQUUsVUFBTyxJQUFJLGlCQUFhO1FBQ2pDLGNBQWMsRUFBRSxpQ0FFTCxJQUFJLFlBQU8sWUFBYztLQUNyQyxDQUFBO0FBQ0gsQ0FBQyxDQUFBIn0=